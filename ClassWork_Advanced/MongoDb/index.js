const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose')
const defaultRoute = require('./routs/default');
const clientsRouter = require('./routs/clients')
const paymentsRouter = require('./routs/payments')

const app = express();
const config = dotenv.config().parsed;



try {

    
    mongoose
        .connect(config.DB_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true

        })
        .then(() => {
            app.set('view engine', config.TEMPLATE_ENGINE)
            app.set('views', config.TEMPLATE_VIEW_PATH)

            app.use(express.static(config.PUBLIC_ROOT))

            app.use('/', defaultRoute());
            app.use('/clients', clientsRouter());
            app.use('/payments', paymentsRouter());

            app.listen(config.PORT, err => {
                if (err) {
                    throw new Error(err)
                }

                console.log('We are good!!!')
            })
    })
    .catch(error => {
        console.log("ERROR!!!!", error)
    })


} catch (e) {
    console.log(e)
}

