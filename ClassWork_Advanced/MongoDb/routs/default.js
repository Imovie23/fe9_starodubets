const router = require('express').Router()
const Clients = require('../models/client')

module.exports = function () {
    
    router.get('/', function (req, res) {

        Clients.find({rate: {$gte: 100}})
            .sort({rate: 'desc'})
            .limit(3)
            .exec(function (err, clients) {
                res.render('index', {
                    title: 'Super Site',
                    clients
                })
            })

    })

    return router
}