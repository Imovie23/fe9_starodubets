const router = require('express').Router()
const Payment = require('../models/payment')

module.exports = function () {

    router.get('/', function (req, res) {
        Payment
            .find({})
            .sort({rate: 'desc'})
            .limit(5)
            .exec(function (err, payment) {
                res.json(payment)
            })
    })

    router.post('/', function (req, res) {

        const balance = new Payment({
            clientId: "5d907eeb7696b14e96105063",
            description: "sssss",
            balance: 1000,
            sum: 300,
            created_at: new Date(),
            updated_at: new Date()
        });

        balance.save(function (err) {
            if(err) throw new Error(err);

            res.json(balance)
        })


    })

    return router
}