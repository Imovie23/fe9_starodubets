const router = require('express').Router()
const Client = require('../models/client')

module.exports = function () {
    
    router.get('/', function (req, res) {
        Client
            .find({})
            .sort({rate: 'desc'})
            .limit(5)
            .exec(function (err, clients) {
            res.json(clients)
        })

    });
    
    router.post('/', function (req, res) {

        const chris = new Client({
            firstName: 'Chrise',
            lastName: 'Post',
            location: 'Kiev',
            meta: {
                age: 95
            },
            reta: 1200,
            created_at: new Date(),
            updated_at: new Date()
        });

        chris.save(function (err) {
            if(err) throw new Error(err);

            res.json(chris)
        })


    });

    return router
};