const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const paymentSchema = new Schema({
    clientId: {type: String, required: true},
    description: {type: String, required: true},
    balance: Number,
    sum: {type: Number, required: true},
    created_at: Date,
    updated_at: Date

})

const Payment = mongoose.model('Payment', paymentSchema);

module.exports = Payment;