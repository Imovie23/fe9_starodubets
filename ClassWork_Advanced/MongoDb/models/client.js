const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const clientSchema = new Schema({
    firstName: {type: String, required: true},
    lastName: String,
    location: String,
    meta: {
        age: {type: Number, required: true},
        website: String
    },
    reta: Number,
    created_at: Date,
    updated_at: Date

})

const Client = mongoose.model('Client', clientSchema);

module.exports = Client;