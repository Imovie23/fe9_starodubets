$(function () {

    const dropDownMenu = $('.navigation__menu');


    dropDownMenu.on('click', function (e) {
        const event = e.currentTarget;
        const line = $('.navigation__line');
        const close = $('.fas.fa-times');
        const menuLook = $('.navigation__categories.navigation__categories--hover');

        if (event === dropDownMenu[0]) {
            console.log(menuLook.css('display'));

            if (menuLook.css('display') === 'none') {
                menuLook.css('display', 'block');
                line.css('display', 'none');
                close.css('display', 'inline-block');

            } else {
                menuLook.css('display', 'none');
                line.css('display', 'flex');
                close.css('display', 'none');
            }

        }


    });


});