document.addEventListener('DOMContentLoaded', onReady);


/**
 * @desc Функция для запуска скрипта после загрузки всего DOM
 *
 */
function onReady() {

    const table = document.getElementById('table-parent');
    const body = document.querySelector('body');
    let numCol = 30;
    let numRow = 30;

    if (!table) {
        const table = document.createElement('table');
        table.classList.add('table');
        table.id = 'table';
        table.setAttribute('cellpadding', '0');
        table.setAttribute('cellspacing', '0');
        body.appendChild(table);

        for (let i = 0; i < numCol; i++) {
            const column = document.createElement('tr');
            column.classList.add('table__column');

            for (let j = 0; j < numRow; j++) {
                const row = document.createElement('td');
                row.classList.add('table__row');
                column.appendChild(row);
            }

            table.appendChild(column);
        }

    }

    body.addEventListener('click', onBodyClick);

    /**
     * @desc Функция event click для изменения цвета клеток на противоположный
     */
    function onBodyClick(e) {
        let element = e.target;
        const tableActive = document.getElementsByClassName('table');

        if (element === body) {
            if (tableActive[0].classList.contains('table-active')) {
                tableActive[0].classList.remove('table-active');

            } else {
                tableActive[0].classList.add('table-active');
            }
        } else if (element.classList.contains('table__row')) {
            if (element.classList.contains('table__row--active')) {
                element.classList.remove('table__row--active')
            } else {
                element.classList.add('table__row--active');
            }
        }


    }

}