
/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

function Hamburger(size, stuffing) {
    this.size = size;
    this.stuffing = stuffing;
    this.topping = [];


    if(!this.size || !this.stuffing){
        throw new HamburgerException('no size given');
    }else if (this.size !== Hamburger.SIZE_SMALL && this.size !== Hamburger.SIZE_LARGE) {
        throw new HamburgerException('invalid size' + ' ' + '\'TOPPING_SAUCE\'');
    }
}

Hamburger.SIZE_SMALL = 'SMALL';
Hamburger.SIZE_LARGE = 'LARGE';
Hamburger.STUFFING_CHEESE = 'CHEESE';
Hamburger.STUFFING_SALAD = 'SALAD';
Hamburger.STUFFING_POTATO = 'POTATO';
Hamburger.TOPPING_MAYO = 'MAYO';
Hamburger.TOPPING_SPICE = 'SPICE';


Hamburger.prototype.items = function () {
    return {
        'SMALL': {price: 50, calories: 20},
        'LARGE': {price: 100, calories: 40},
        'CHEESE': {price: 10, calories: 20},
        'SALAD': {price: 20, calories: 5},
        'POTATO': {price: 15, calories: 10},
        'MAYO': {price: 20, calories: 5},
        'SPICE': {price: 15, calories: 0}
    }
};

/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.addTopping = function (topping) {

    if (topping === undefined) {
        throw new HamburgerException(topping + ' ' + 'no size given');
    } else if (this.topping.includes(topping)) {
        throw new HamburgerException('duplicate topping' + ' ' + topping);
    } else {
        if (topping === Hamburger.TOPPING_MAYO) {
            return this.topping.push(Hamburger.TOPPING_MAYO);
        } else if (topping === Hamburger.TOPPING_SPICE) {
            return this.topping.push(Hamburger.TOPPING_SPICE);
        }
    }
};

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */

Hamburger.prototype.removeTopping = function (topping) {

    for(var i = 0; i < this.topping.length; i++){
       if(this.topping[i] === topping) {
           return this.topping.splice(this.topping.indexOf(topping), 1)
       }
    }
};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */

Hamburger.prototype.getToppings = function () {
    return this.topping.map(function (i) {
        return i;
    });
};

/**
 * Узнать размер гамбургера
 */

Hamburger.prototype.getSize = function () {
    return this.size;
};

/**
 * Узнать начинку гамбургера
 */

Hamburger.prototype.getStuffing = function () {
    return this.stuffing;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */

Hamburger.prototype.calculatePrice = function () {

    var priceCalc = hamburger.items();

    var toppingElements = this.topping.reduce(function (a, b) {

        return a + priceCalc[b].price;

    }, 0);


    return priceCalc[this.size].price + priceCalc[this.stuffing].price + +toppingElements;
};

/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */


Hamburger.prototype.calculateCalories = function () {

    var caloriesCalc = hamburger.items();

    var toppingElements = this.topping.reduce(function (a, b) {

        return a + caloriesCalc[b].calories;

    }, 0);

    return caloriesCalc[this.size].calories + caloriesCalc[this.stuffing].calories + toppingElements;

};

/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */

function HamburgerException(messege) {
    this.name = 'HamburgerException';
    this.message = messege;

}


try {


//маленький гамбургер с начинкой из сыра
    var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);

    console.dir(hamburger);
// добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);

// А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1
    console.dir(hamburger);

} catch (e) {

    alert(e.name + ' : ' + e.message);
}




