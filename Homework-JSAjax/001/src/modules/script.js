import '../modules/classes/system/API'
import Films from '../modules/classes/services/films'

document.addEventListener('DOMContentLoaded', onReady);


function onReady() {
    const btnGetXMLHttpRequest = document.getElementById('getFilmClick');
    btnGetXMLHttpRequest.addEventListener('click', btnGetXMLHttpRequestClick)
}

function btnGetXMLHttpRequestClick() {
    let textStarWars = document.getElementById('text')
    
    if(!textStarWars) {
        textStarWars = document.createElement('div')
        textStarWars.id = 'text'
        document.body.append(textStarWars)
       
    }else {
        textStarWars.remove()
        textStarWars = document.createElement('div')
        textStarWars.id = 'text'
        document.body.append(textStarWars)
    }

    const films = new Films();

    films.getFilmsByXMLHttpReques('https://swapi.co/api/films/', onDataFilms)


    function onLinkCharacters(item) {

       return films.getFilmsByXMLHttpReques(item, (response) => {})
    }

    function onDataFilms(respons) {
        let arr = []
        let arrItem = []
        if (respons ) {

            if(Array.isArray(respons.results)){
                    arr = respons.results
                arr.forEach(i => {
                    const div =  document.createElement('div')
                    div.id = ('parrent')
                    div.innerHTML = `<h1>Episode ${i.episode_id}</h1> <h2>${i.title}</h2> <p>${i.opening_crawl}</p>`
                    const characters = document.createElement('ul')
                    characters.id = 'list-characters'
                    div.append(characters)
                    textStarWars.append(div)

                    arrItem = i.characters.map(link => {
                       return onLinkCharacters(link)

                    })

                allCharacters(arrItem, respons => {

                   if(Array.isArray(respons)){
                    respons.forEach(i => {
                        const li = document.createElement('li')
                        characters.append(li)
                        li.innerHTML = i.name
                    })
                    }else {
                    characters.append(respons)
                }
                })
            })
            }
        }
    }


    function allCharacters(request, callback) {

        let count = 0;
        const data = [];

            request.forEach(item => {

                item.addEventListener('load', function () {
                    count++;
                    data.push(JSON.parse(item.response));

                    if (request.length === count) {
                        let container = document.getElementById('container')
                        if(container) {
                            container.remove()
                        }

                        callback(data);
                    }else {
                        let container = document.getElementById('container')
                        if(!container) {
                            container = document.createElement('div')
                            container.classList.add('container')
                            container.id = 'container';
                            const item1 = document.createElement('div')
                            item1.classList.add('item-1')
                            const item2 = document.createElement('div')
                            item2.classList.add('item-2')
                            const item3 = document.createElement('div')
                            item3.classList.add('item-3')
                            const item4 = document.createElement('div')
                            item4.classList.add('item-4')
                            const item5 = document.createElement('div')
                            item5.classList.add('item-5')

                            container.append(item1)
                            container.append(item2)
                            container.append(item3)
                            container.append(item4)
                            container.append(item5)
                            callback(container)
                        }

                    }
                })
           })
    }

}







