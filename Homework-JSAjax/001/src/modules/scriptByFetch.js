import '../modules/classes/system/API'
import Films from '../modules/classes/services/films'


document.addEventListener('DOMContentLoaded', onReady);


function onReady() {
    const btnGetFetch = document.getElementById('getFilmClickFetch');
    btnGetFetch.addEventListener('click', btnGetFetchClick)

}

function btnGetFetchClick() {

    let textStarWars = document.getElementById('text')

    if(!textStarWars) {
        textStarWars = document.createElement('div')
        textStarWars.id = 'text'
        document.body.append(textStarWars)
    }else {
        textStarWars.remove()
        textStarWars = document.createElement('div')
        textStarWars.id = 'text'
        document.body.append(textStarWars)
    }
    

    const films = new Films();

    async function getByFetch() {
        let filmsResult = await films.getFilmByFetch('https://swapi.co/api/films/')
        return filmsResult.results
    }


    async function render() {
        let filmsItem = await getByFetch();


        filmsItem.forEach(async function (item) {
            const div =  document.createElement('div')
            
            div.id = ('parrent')
            div.innerHTML = `<h1>Episode ${item.episode_id}</h1> <h2>${item.title}</h2> <p>${item.opening_crawl}</p>`
            const characters = document.createElement('ul')
            characters.id = 'list-characters'
            div.append(characters)
            textStarWars.append(div)

            let container = document.getElementById('container')
                container = document.createElement('div')
                container.classList.add('container')
                container.id = 'container';
                const item1 = document.createElement('div')
                item1.classList.add('item-1')
                const item2 = document.createElement('div')
                item2.classList.add('item-2')
                const item3 = document.createElement('div')
                item3.classList.add('item-3')
                const item4 = document.createElement('div')
                item4.classList.add('item-4')
                const item5 = document.createElement('div')
                item5.classList.add('item-5')

                container.append(item1)
                container.append(item2)
                container.append(item3)
                container.append(item4)
                container.append(item5)
                characters.append(container)


            const res = await Promise.all(getCharacters(item.characters));

            res.forEach(name => {
                container.remove()
                let li = document.createElement('li')
                li.innerHTML = name
                characters.append(li)
           })
        })

    }

    render()


     function getCharacters(character) {

         let arr =[]

        arr.push(character.forEach(item => {
            arr.push(films.getFilmByFetch(item)
               .then(result => {
                  return result.name
            }))
       }))
         arr.pop()


         return arr
    }

}