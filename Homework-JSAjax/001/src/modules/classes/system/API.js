import script from '../../script'


class Api {
    constructor(server) {
    }

    getUrl(url) {
        return url
}


    getByXMLHttpRequest(url, callback){

        let request = new XMLHttpRequest;

        

        request.open('GET', this.getUrl(url))

        request.onreadystatechange = function() {
            if (request.readyState === 4 && request.status === 200) {
                
                if(typeof callback === 'function'){
                  callback(JSON.parse(request.response))
                }
                

            }

            

        }
        
        request.send();

        return request
    }

    getByFetch(url) {
            return fetch(url, {
                type: 'GET'
            }).then(response => response.json())
            .catch(reject => new Error('No items'))
    }

  
    
    
    
}

export default Api