import Api from '../system/API'

class Films extends Api {

    constructor(server) {
        super(server);

    }


    getFilmsByXMLHttpReques(url, callback) {
       return  super.getByXMLHttpRequest(url, callback)

    }
    
    getFilmByFetch(url){
        return super.getByFetch(url)
    }
    

}

export default Films