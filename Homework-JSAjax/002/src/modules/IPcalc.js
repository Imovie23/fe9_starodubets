import CalculateByIP from './classes/services/CalculateByIP'

document.addEventListener('DOMContentLoaded', onReady)


function onReady() {

    let ipCalc = new CalculateByIP

    const btn = document.getElementById('getIPClick')
    btn.addEventListener('click', onBtnClickGetIP)

function onBtnClickGetIP () {
        render()
}


     function getIP () {
         return ipCalc.getIPByFetch('https://api.ipify.org/?format=json');
    }

    async function postContent() {
        let ipFetch = await getIP();
        return  await ipCalc.getIPByFetch(`http://ip-api.com/json/${ipFetch.ip}?lang=ru&fields=district,continent,country,city,regionName`)
    }

    async function render()  {
        let dataIP = await postContent();
        console.log(dataIP);
        Object.entries(dataIP).map(([key, value]) =>{
            let div = document.createElement('div')
            div.innerHTML = `${key} : ${value}`.toUpperCase();
            document.body.append(div)
        })

    }
}