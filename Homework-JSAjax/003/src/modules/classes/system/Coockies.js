class Coockies {
    constructor(name, value) {
        this.name = name;
        this.value = value

    }
    

    creatCookies() {
        document.cookie = `${this.name}=${ this.value}`
    }

    getCookies(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    
     setCookie(name, value, options = {}) {
        
         options = {
            ...options
        };

        let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

         for (let optionKey in options) {
             updatedCookie += "; " + optionKey;
             let optionValue = options[optionKey];
             if (optionValue !== true) {
                updatedCookie += "=" + optionValue;
             }
        }
         
         document.cookie = updatedCookie;
     }


     deleteCookie(name) {
        this.setCookie(name, "", {
            'max-age': -1
        })
    }
    
    replaceCookie(name, value){
        this.setCookie(name, value)
    }

    
}

export default Coockies