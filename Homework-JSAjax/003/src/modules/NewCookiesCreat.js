import NewUser from './classes/services/NewUser'

document.addEventListener('DOMContentLoaded', onReady)
let btn = document.getElementById('button-black')


function onReady() {
    btn.addEventListener('click', onBtnClick);
}



function onBtnClick() {
    cookiesCreate()
}



function cookiesCreate() {
    const newUser = new NewUser('new-user', 'true')

    const findCookieUser = newUser.getCookies('new-user');
    const findCookieExperiment = newUser.getCookies('experiment');

    findCookieUser !== undefined ?  newUser.replaceCookie('new-user', false) : newUser.creatCookies()

    if(findCookieExperiment === undefined){
        newUser.setCookie('experiment', 'novalue', {'max-age': 300});
    }


    console.log(document.cookie);
}